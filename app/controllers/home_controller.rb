require 'open-uri'

class HomeController < ApplicationController
  respond_to :json
  before_filter :authenticate_user!

  def index
    url = URI::encode("http://chrysler-ram-server-prod.herokuapp.com/api/contatos/")
    open(url) do |s|
      @result = JSON.parse(s.read)
    end

    if @result == nil

    else
      @contacts = @result["contacts"]
    end

  end

  def show
    url = URI::encode("http://chrysler-ram-server-prod.herokuapp.com/api/contatos/#{params[:id]}")
    open(url) do |s|
      @result = JSON.parse(s.read)
    end

    if @result == nil

    else
      @contact = @result["contact"]
    end
  end
end
