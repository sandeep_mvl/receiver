# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/
jQuery ->
  $('#contacts').dataTable
    sPaginationType: "bootstrap"
    bJQueryUI: true
    order: [[ 7, "desc" ]]
    language: {
      "lengthMenu": " _MENU_ registros por página",
      "zeroRecords": "Nada foi encontrado",
      "info": "Mostrando página _PAGE_ de _PAGES_",
      "infoEmpty": "Não há registros disponíveis",
      "infoFiltered": "(filtered from _MAX_ total records)"
    }