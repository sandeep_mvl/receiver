Receiver::Application.routes.draw do

  devise_for :users, :controllers => { :registrations => "registrations" }

  get "home/index"

  match "contact/:id" => 'home#show'

  root :to => 'home#index'

end
